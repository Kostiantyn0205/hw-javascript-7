/*1)Для кожного елементу масиву викликається передана в метод функція, і має доступ до значення елемента та його індексу.
Метод forEach не повертає жодного значення, він тільки перебирає елементи масиву та застосовує передану функцію до кожного з них*/
/*2)Можна встановити довжину массива в 0: array.length = 0; або Використати метод splice: array.splice(0, array.length); або Призначити порожній масив для змінної, що містить масив: array = []*/
/*3)Для перевірки можна використати методу Array.isArray()*/

let filterBy = function (arr, type) {
    let newArray = [];
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] !== type) {
            newArray.push(arr[i]);
        }
    }
    return newArray;
}

let array = [true, 'hello', 17, 'world', 23, '23', null, false, {}];
console.log(filterBy(array, 'string'));
console.log(filterBy(array, 'number'));
console.log(filterBy(array, 'boolean'));
console.log(filterBy(array, 'object'));